import React, { useState } from 'react';
import "./chatBody.scss";
import ChatLists from "../chatList/ChatLists/ChatLists.jsx";
import Chats from "../chatContent/Chats/Chats";
import UserProfile from "../userProfile/UserProfile.jsx";


const ChatBody = () => {

  const [userProfile, setUserProfile] = useState(false);

  const toggleProfile = () => {
    setUserProfile(!userProfile);
  };

  return (
    <div className="main__chatbody">
      <ChatLists />
      <Chats toggleProfile={toggleProfile} userProfile={userProfile} />
      <UserProfile userProfile={userProfile} />
    </div>
  );
};

export default ChatBody;