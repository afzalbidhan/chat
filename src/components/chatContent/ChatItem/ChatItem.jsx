import React from 'react';
import Avatar from '../../Avatar/Avatar.jsx';

const ChatItem = ({ user, msg, image, name }) => {
  return (
    <div style={{ animationDelay: `0.8s` }} className={`chat__item ${user ? user : ""}`} >
      <div className="chat__item__content">
        <div className="chat__msg">{msg}</div>
        {
          user === "other" ?
            <div className="chat__meta">
              <span>sent by {name}, 5:51PM</span>
            </div>
            :
            <div className="chat__meta">
              <span>5:51PM</span>
            </div>
        }

      </div>
      <Avatar isOnline="active" image={image} />
    </div>
  );
};

export default ChatItem;