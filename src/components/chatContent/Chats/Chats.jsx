import React, { useRef, useState } from 'react';
import img6 from '../../../Assets/Images/avatar/img-6.png'
import gggImg from '../../../Assets/Images/group/img-1.jpg'
import { useEffect } from 'react';
import ChatItem from '../ChatItem/ChatItem.jsx';
import Avatar from '../../Avatar/Avatar.jsx';
import './chats.scss';
import { chatCont } from '../../../data';

const ChatContent = ({ toggleProfile, userProfile }) => {

  const messagesEndRef = useRef(null);

  const [chat, setChat] = useState(chatCont);
  const [msg, setMsg] = useState("");
  const [newChat, setNewChat] = useState({});


  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };


  const onStateChange = (e) => {
    setMsg(e.target.value);
    setNewChat({
      key: 9,
      type: "",
      msg: e.target.value,
      image: img6,
    });
  };


  const handleSubmit = (e) => {
    e.preventDefault();
    setChat([...chat, newChat]);
    setMsg("");
  };

  useEffect(() => {
    scrollToBottom();
  }, [chat]);


  return (
    <div className="main__chatcontent">
      <div className="content__header">
        <div className="blocks">
          <div className="current-chatting-user">
            <Avatar
              isOnline="active"
              image={gggImg}
            />
            <p>Scholarcave</p>
          </div>
        </div>

        <div className="blocks">
          <div className="settings">
            <button className="btn-nobg" onClick={toggleProfile}>
              <i className={userProfile ? "fas fa-times" : "fa fa-cog"}></i>
            </button>
          </div>
        </div>
      </div>
      <div className="content__body">
        <div className="chat__items">
          {chat.map((itm, index) => {
            return (
              <ChatItem
                animationDelay={index + 2}
                key={itm.key}
                user={itm.type ? itm.type : "me"}
                msg={itm.msg}
                image={itm.image}
                name={itm.name}
              />
            );
          })}
          <div ref={messagesEndRef} />
        </div>
      </div>
      <div className="content__footer">
        <div className="sendNewMessage">
          <button className="addFiles">
            <i className="fa fa-plus"></i>
          </button>
          <form onSubmit={handleSubmit}>
            <input
              type="text"
              placeholder="Type a message here"
              onChange={onStateChange}
              name="msg"
              value={msg}
            />
            <button type="submit" className="btnSendMsg" id="sendMsgBtn">
              <i className="fa fa-paper-plane"></i>
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ChatContent;