import React from 'react';
import Avatar from '../../Avatar/Avatar.jsx';
import './chatListItem.scss';

const ChatListItem = ({ animationDelay, active, image, isOnline, name, sms }) => {

  const selectChat = (e) => {
    for (
      let index = 0;
      index < e.currentTarget.parentNode.children.length;
      index++
    ) {
      e.currentTarget.parentNode.children[index].classList.remove("active");
    }
    e.currentTarget.classList.add("active");
  };

  return (
    <div
      style={{ animationDelay: `0.${animationDelay}s` }}
      onClick={selectChat}
      className={`chatlist__item ${active ? active : ""
        } `}
    >
      <Avatar
        image={image}
        isOnline={isOnline}
      />

      <div className="userMeta">
        <p>{name}</p>
        <span className="activeTime">{sms}</span>
      </div>
    </div>
  );
};

export default ChatListItem;