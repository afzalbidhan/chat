import React from 'react';
import "./chatLists.scss";
import ChatListItem from '../ChatListItem/ChatListItem';
import { chatListData } from './../../../data';

const ChatLists = () => {
  return (
    <div className="main__chatlist">

      <div className="chatlist__heading">
        <h2>Chats</h2>
        <button className="btn-nobg">
          <i className="fa fa-ellipsis-h"></i>
        </button>
      </div>
      <div className="chatList__search">
        <div className="search_wrap">
          <input type="text" placeholder="Search Here" required />
          <button className="search-btn">
            <i className="fa fa-search"></i>
          </button>
        </div>
      </div>
      <div className="chatlist__items">
        {chatListData.map((item, index) => {
          return (
            <ChatListItem
              name={item.name}
              key={item.id}
              animationDelay={index + 1}
              active={item.active ? "active" : ""}
              isOnline={item.isOnline ? "active" : ""}
              image={item.image}
              sms={item.sms}
            />
          );
        })}
      </div>
    </div>
  );
};

export default ChatLists;