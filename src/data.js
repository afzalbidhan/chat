import imgCl1 from './Assets/Images/group/img-1.jpg';
import imgCl2 from './Assets/Images/group/img-2.png';
import imgCl3 from './Assets/Images/group/img-3.png';
import imgCl4 from './Assets/Images/group/img-4.png';
import imgCl5 from './Assets/Images/group/img-5.jpg';
import imgCl6 from './Assets/Images/group/img-6.jpg';
import imgCl7 from './Assets/Images/group/img-7.jpg';
import imgCl8 from './Assets/Images/group/img-8.png';
import imgCl9 from './Assets/Images/group/img-9.png';
import imgCl10 from './Assets/Images/group/img-10.jpg';

import imgCC1 from './Assets/Images/avatar/img-1.png';
import imgCC2 from './Assets/Images/avatar/img-2.png';
import imgCC3 from './Assets/Images/avatar/img-3.png';
import imgCC4 from './Assets/Images/avatar/img-4.png';
import imgCC6 from './Assets/Images/avatar/img-6.png';





export const chatListData = [
    {
        image: imgCl1,
        id: 1,
        name: "Scholarcave",
        active: true,
        isOnline: true,
        sms: "Created chat ui 👌👌"

    },
    {
        image: imgCl2,
        id: 2,
        name: "Bidhan",
        active: false,
        isOnline: true,
        sms: "How are you??"
    },

    {
        image: imgCl3,
        id: 10,
        name: "Manpreet David",
        active: false,
        isOnline: true,
        sms: "No it does not"
    },
    {
        image: imgCl4,
        id: 8,
        name: "Autumn Mckee",
        active: false,
        isOnline: false,
        sms: "Just be very careful doing that"
    },
    {
        image: imgCl5,
        id: 3,
        name: "Hamaad Dejesus",
        active: false,
        isOnline: false,
        sms: "Awesome!!! Congratulations!!!!"
    },
    {
        image: imgCl6,
        id: 4,
        name: "Eleni Hobbs",
        active: false,
        isOnline: true,
        sms: "Good job👍👍"
    },
    {
        image: imgCl7,
        id: 5,
        name: "Elsa Black",
        active: false,
        isOnline: false,
        sms: "Thank you. I appreciate that."
    },
    {
        image: imgCl8,
        id: 6,
        name: "Kayley Mellor",
        active: false,
        isOnline: true,
        sms: "Open a new project"
    },
    {
        image: imgCl9,
        id: 7,
        name: "Hasan Mcculloch",
        active: false,
        isOnline: true,
        sms: "Message was removed"
    },
    {
        image: imgCl10,
        id: 9,
        name: "Allen Woodley",
        active: false,
        isOnline: true,
        sms: "Just Hello"
    },
];


export const chatCont = [
    {
      key: 1,
      image: imgCC6,
      type: "",
      msg: "Hi Tim, How are you?",
    },
    {
      key: 2,
      image: imgCC1,
      type: "other",
      name: "Bidhan",
      msg: "I am fine.",
    },
    {
      key: 3,
      image: imgCC2,
      type: "other",
      name: "Sabbir",
      msg: "What about you?",
    },
    {
      key: 4,
      image: imgCC6,
      type: "",
      msg: "Awesome these days.",
    },
    {
      key: 5,
      image: imgCC3,
      type: "other",
      name: "Mridul",
      msg: "Finally. What's the plan?",
    },
    {
      key: 6,
      image: imgCC6,
      type: "",
      msg: "what plan mate?",
    },
    {
      key: 7,
      image: imgCC4,
      type: "other",
      name: "Rakib",
      msg: "Ok fair enough. Well good talking to you.",
    },
    {
      key: 7,
      image: imgCC1,
      type: "other",
      name: "Bidhan",
      msg: "Created chat ui 👌👌",
    },
  ];