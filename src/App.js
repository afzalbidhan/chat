import React from "react";
import "./app.scss";
import ChatBody from './components/chatBody/ChatBody.jsx';

function App() {
  return (
    <div className="__main">
      <ChatBody />
    </div>
  );
}

export default App;
